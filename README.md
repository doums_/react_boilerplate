## Fresh and minimalist React boilerplate
**Built using [webpack](https://webpack.js.org/)**

***Goal***
> *Make a simple and lightweight boilerplate from scratch*

***main features***
 - Hot Module Replacement (HMR)
 - React Hot Loader
 - ESLint
 - Standard JS
 - DevServer (webpack-dev-server)
 - Source Maps

***installation***
`$ git clone https://doums_@bitbucket.org/doums_/react_boilerplate.git`

***run for development***
`$ npm start`

***build for production***
`$ npm run build`